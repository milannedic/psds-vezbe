import os
import sys
from jinja2 import Environment, FileSystemLoader

PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_ENVIRONMENT = Environment(
    autoescape=False,
    loader=FileSystemLoader(os.path.join(PATH, 'templates')),
    trim_blocks=True)


def render_template(template_filename, context):
    return TEMPLATE_ENVIRONMENT.get_template(template_filename).render(context)


def render_testbench(entity_name, context, fname):

    with open(fname, 'w') as f:
        hdl = render_template('testbench.vhd', context)
        f.write(hdl)


def main(argv):
    entity_name = argv[1]
    render_testbench(entity_name)


if __name__ == "__main__":
    main(sys.argv)
