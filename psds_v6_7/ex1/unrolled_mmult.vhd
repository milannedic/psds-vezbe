library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity unrolled_mmult is
  generic (
    WIDTH : integer := 8;
    SIZE  : integer := 3
    );
  port (
    --------------- Clocking and reset interface ---------------
    clk       : in  std_logic;
    reset     : in  std_logic;
    ------------------- Input data interface -------------------
    -- Matrix A memory interface, port 1
    a1_addr_o : out std_logic_vector(log2c(SIZE*SIZE)-1 downto 0);
    a1_data_i : in  std_logic_vector(WIDTH-1 downto 0);
    a1_wr_o   : out std_logic;
    -- Matrix A memory interface, port 2
    a2_addr_o : out std_logic_vector(log2c(SIZE*SIZE)-1 downto 0);
    a2_data_i : in  std_logic_vector(WIDTH-1 downto 0);
    a2_wr_o   : out std_logic;
    -- Matrix B memory interface
    b_addr_o  : out std_logic_vector(log2c(SIZE*SIZE)-1 downto 0);
    b_data_i  : in  std_logic_vector(WIDTH-1 downto 0);
    b_wr_o    : out std_logic;
    -- Matrix dimensions definition interface
    n_in      : in  std_logic_vector(log2c(SIZE)-1 downto 0);
    p_in      : in  std_logic_vector(log2c(SIZE)-1 downto 0);
    m_in      : in  std_logic_vector(log2c(SIZE)-1 downto 0);
    ------------------- Output data interface ------------------
    -- Matrix C memory interface, port 1
    c1_addr_o : out std_logic_vector(log2c(SIZE*SIZE)-1 downto 0);
    c1_data_o : out std_logic_vector(2*WIDTH+SIZE-1 downto 0);
    c1_wr_o   : out std_logic;
    -- Matrix C memory interface, port 2
    c2_addr_o : out std_logic_vector(log2c(SIZE*SIZE)-1 downto 0);
    c2_data_o : out std_logic_vector(2*WIDTH+SIZE-1 downto 0);
    c2_wr_o   : out std_logic;
    --------------------- Command interface --------------------
    start     : in  std_logic;
    --------------------- Status interface ---------------------
    ready     : out std_logic);
end entity;
  architecture two_seg_arch of unrolled_mmult is
  type state_type is (idle, l1, l2, l3);
  signal state_reg, state_next : state_type;
  signal i_reg, i_next         : unsigned(log2c(SIZE)-1 downto 0);
  signal j_reg, j_next         : unsigned(log2c(SIZE)-1 downto 0);
  signal k_reg, k_next         : unsigned(log2c(SIZE)-1 downto 0);
  signal temp1_reg, temp1_next : unsigned(2*WIDTH+SIZE-1 downto 0);
  signal temp2_reg, temp2_next : unsigned(2*WIDTH+SIZE-1 downto 0);
begin
  -- State and data registers
  process (clk, reset)
  begin
    if reset = '1' then
      state_reg <= idle;
      i_reg     <= (others => '0');
      j_reg     <= (others => '0');
      k_reg     <= (others => '0');
      temp1_reg <= (others => '0');
      temp2_reg <= (others => '0');
    elsif (clk'event and clk = '1') then
      state_reg <= state_next;
      i_reg     <= i_next;
      j_reg     <= j_next;
      k_reg     <= k_next;
      temp1_reg <= temp1_next;
      temp2_reg <= temp2_next;
    end if;
  end process;

  -- Combinatorial circuits
  process (state_reg, start, a1_data_i, a2_data_i, b_data_i, i_reg, j_reg, k_reg,
           temp1_reg, temp2_reg, i_next, j_next, k_next, temp1_next, temp2_next)
    variable conv_v : std_logic_vector(2*log2c(SIZE)-1 downto 0);
  begin
    -- Default assignments
    i_next     <= i_reg;
    j_next     <= j_reg;
    k_next     <= k_reg;
    temp1_next <= temp1_reg;
    temp2_next <= temp2_reg;
    a1_addr_o  <= (others => '0');
    a2_addr_o  <= (others => '0');
    a1_wr_o    <= '0';
    a2_wr_o    <= '0';
    b_addr_o   <= (others => '0');
    b_wr_o     <= '0';
    c1_addr_o  <= (others => '0');
    c2_addr_o  <= (others => '0');
    c1_wr_o    <= '0';
    c2_wr_o    <= '0';
    ready      <= '0';

    case state_reg is
      when idle =>
        ready <= '1';
        if start = '1' then
          i_next     <= to_unsigned(0, log2c(SIZE));
          state_next <= l1;
        else
          state_next <= idle;
        end if;

      when l1 =>
        j_next     <= to_unsigned(0, log2c(SIZE));
        state_next <= l2;

      when l2 =>
        temp1_next <= to_unsigned(0, 2*WIDTH+SIZE);
        temp2_next <= to_unsigned(0, 2*WIDTH+SIZE);
        k_next     <= to_unsigned(0, log2c(SIZE));
        conv_v     := std_logic_vector(i_reg*unsigned(n_in)+k_next);
        a1_addr_o  <= conv_v(a1_addr_o'range);
        if (i_reg < unsigned(n_in) - 1) then
          conv_v    := std_logic_vector((i_reg+1)*unsigned(n_in)+k_next);
          a2_addr_o <= conv_v(a2_addr_o'range);
        end if;
        conv_v     := std_logic_vector(k_next*unsigned(m_in)+j_reg);
        b_addr_o   <= conv_v(b_addr_o'range);
        state_next <= l3;
      when l3 =>
        temp1_next <= temp1_reg + unsigned(a1_data_i)*unsigned(b_data_i);
        temp2_next <= temp2_reg + unsigned(a2_data_i)*unsigned(b_data_i);
        k_next     <= k_reg + 1;
        conv_v     := std_logic_vector(i_reg*unsigned(n_in)+k_next);
        a1_addr_o  <= conv_v(a1_addr_o'range);
        if (i_reg < unsigned(n_in) - 1) then
          conv_v    := std_logic_vector((i_reg+1)*unsigned(n_in)+k_next);
          a2_addr_o <= conv_v(a2_addr_o'range);
        end if;
        conv_v   := std_logic_vector(k_next*unsigned(m_in)+j_reg);
        b_addr_o <= conv_v(b_addr_o'range);
        if (k_next = unsigned(m_in)) then
          conv_v    := std_logic_vector(i_reg*unsigned(n_in)+j_reg);
          c1_addr_o <= conv_v(c1_addr_o'range);
          c1_data_o <= std_logic_vector(temp1_next);
          c1_wr_o   <= '1';
          conv_v    := std_logic_vector((i_reg+1)*unsigned(n_in)+j_reg);
          c2_addr_o <= conv_v(c2_addr_o'range);
          c2_data_o <= std_logic_vector(temp2_next);
          if (i_reg < unsigned(n_in) - 1) then
            c2_wr_o <= '1';
          end if;
          j_next <= j_reg + 1;
          if (j_next = unsigned(p_in)) then
            i_next <= i_reg + 2;
            if (i_next < unsigned(n_in)) then
              state_next <= l1;
            else
              state_next <= idle;
            end if;
          else
            state_next <= l2;
          end if;
        else
          state_next <= l3;
        end if;
    end case;
  end process;
end two_seg_arch;
