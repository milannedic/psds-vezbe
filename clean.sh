echo -e "\n\n\t\t -- Removing all .o files"
find . -name '*.o' -delete	# clean all .o files

echo -e "\t\t -- Removing all .cf files"
find . -name '*.cf' -delete	# clean all .cf files

echo -e "\t\t -- Removing all .ghw files\n\n"
find . -name '*.ghw' -delete	# clean all .ghw files

execs=$(find ./ -type f | xargs file | grep "ELF.*executable" | awk -F: '{print $1}' | xargs echo)

exec_print="\t\t -- Deleting all executables: $execs"

echo -e "$exec_print \n\n"

# delete executables
find ./ -type f | xargs file | grep "ELF.*executable" | awk -F: '{print $1}' | xargs rm
