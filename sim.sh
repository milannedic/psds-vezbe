# param 
# call this like:
# ./sim.sh psds_v2 2_1 top_level_tb 1000ns wave.gtkw

# this version hides toolbar
# milan@milan:~/gtkwave-3.3.97$ gtkwave /home/milan/PSDS_VEZBE/psds-vezbe/wave.ghw /home/milan/PSDS_VEZBE/psds-vezbe/psds_v3_5/ex3/wave2.gtkw


# params:
#	1 - lesson dir
#	2 - problem folder, e.g 2_2	
#	3 - model to test
#	4 - time interval to simulate
#	5 - gtkw setup file

# echo -e "\n\n\t\t -- Removing old .o files"
# find . -name '*.o' -delete	# clean all .o files

# echo -e "\t\t -- Removing old .cf files"
# find . -name '*.cf' -delete	# clean all .cf files

# echo -e "\t\t -- Removing old .ghw files\n\n"
# find . -name '*.ghw' -delete	# clean all .ghw files

# execs=$(find ./ -type f | xargs file | grep "ELF.*executable" | awk -F: '{print $1}' | xargs echo)

# exec_print="\t\t -- Deleting old executables: $execs"

# echo -e "$exec_print \n\n"

# # delete executables
# find ./ -type f | xargs file | grep "ELF.*executable" | awk -F: '{print $1}' | xargs rm

ghdl -i ./$1/$2/*.vhd
ghdl -i ./util_pkg.vhd	# two directories behind
ghdl -i ./work/*.vhd			
ghdl -m -fexplicit --ieee=synopsys $3

# ghdl -r  $3 --stop-time=$4 --vcd=wave.vcd 
ghdl -r  $3 --stop-time=$4 --wave=wave.ghw

# gtkwave wave.vcd ./$1/$2/$5 # wave.gtkw
gtkwave wave.ghw ./$1/$2/$5 # wave.gtkw


