library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

entity mem_interface is
  generic (
    -- all these generics are actually overrided
    -- by those used in testbench file. This is simply a placeholder!
    DATA_WIDTH : integer := 8;
    MEM_SIZE   : natural := 32
    );
  port (
    --------------- Clocking and reset interface ---------------
    clk       : in  std_logic;
    reset     : in  std_logic;
    -- Memory interface, port 1
    p1_addr_i : in  std_logic_vector(log2c(MEM_SIZE)-1 downto 0);
    p1_data_i : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    p1_data_o : out std_logic_vector(DATA_WIDTH-1 downto 0);
    p1_wr_i   : in  std_logic;
    -- Memory interface, port 2
    p2_addr_i : in  std_logic_vector(log2c(MEM_SIZE)-1 downto 0);
    p2_data_i : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    p2_data_o : out std_logic_vector(DATA_WIDTH-1 downto 0);
    p2_wr_i   : in  std_logic;
    --------------------- Command interface --------------------
    start     : in  std_logic;
    --------------------- Status interface ---------------------
    ready     : out std_logic
    );
end entity;

architecture behavioral of mem_interface is
  type state_type is (idle, writep1, readmem);
  signal state_reg, state_next : state_type;

  signal cnt_next, cnt_reg : std_logic_vector(
    (MEM_SIZE)-1 downto 0) := (others => '0');

  signal addr_next, addr_reg : std_logic_vector(
    log2c(MEM_SIZE)-1 downto 0) := (others => '0');
  signal p1_addr_i_s : std_logic_vector(log2c(MEM_SIZE)-1 downto 0);
  signal p1_data_i_s : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal p1_data_o_s : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal p1_wr_i_s   : std_logic;
  signal p2_addr_i_s : std_logic_vector(log2c(MEM_SIZE)-1 downto 0);
  signal p2_data_i_s : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal p2_data_o_s : std_logic_vector(DATA_WIDTH-1 downto 0);
  signal p2_wr_i_s   : std_logic;


begin
  -- Dual Port Memory instance
  memory_A : entity work.dp_memory(beh)
    generic map (
      WIDTH => DATA_WIDTH,
      SIZE  => MEM_SIZE)
    port map (
      clk       => clk,
      reset     => reset,
      -- Port 1 interface
      p1_addr_i => p1_addr_i_s,
      p1_data_i => p1_data_i_s,
      p1_data_o => p1_data_o_s,
      p1_wr_i   => p1_wr_i,
      -- Port 2 interface
      p2_addr_i => p2_addr_i_s,
      p2_data_i => p2_data_i_s,
      p2_data_o => p2_data_o_s,
      p2_wr_i   => p2_wr_i);

-- State and data registers
  process (clk, reset)
  begin
    if reset = '1' then
      state_reg <= idle;
      cnt_reg   <= (others => '0');
      addr_reg  <= (others => '0');
    elsif (clk'event and clk = '1') then
      state_reg <= state_next;
      cnt_reg   <= cnt_next;
      addr_reg  <= addr_next;
    end if;
  end process;

-- Combinatorial circuits
  process (state_reg, start, cnt_next, cnt_reg, addr_next, addr_reg)
  begin

    -- Default assignments
    cnt_next <= cnt_reg;

    p1_addr_i_s <= (others => '1');
    p2_addr_i_s <= (others => '1');
    p2_addr_i_s <= (others => '0');
    p2_data_i_s <= (others => '0');

    case state_reg is
      when idle =>
        ready <= '1';
        if start = '1' then
          state_next <= writep1;
        else
          state_next <= idle;
        end if;
      when writep1 =>

        cnt_next  <= std_logic_vector(unsigned(cnt_reg) + 1);
        addr_next <= std_logic_vector(unsigned(addr_reg) + 1);

        -- writes 45 and 35, as any values...
        if cnt_reg < std_logic_vector(to_unsigned((MEM_SIZE/2), cnt_reg'length)) then

          p1_addr_i_s <= std_logic_vector(unsigned(addr_reg) + 1);
          p1_data_i_s <= std_logic_vector(to_unsigned(45, p1_data_i_s'length));

        else
          p2_addr_i_s <= std_logic_vector(unsigned(addr_reg) + 1);
          p2_data_i_s <= std_logic_vector(to_unsigned(35, p2_data_i_s'length));
        end if;
        state_next <= writep1;
      when readmem =>
        state_next <= readmem;
    end case;
  end process;
end behavioral;
