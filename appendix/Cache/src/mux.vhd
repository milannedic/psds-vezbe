library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity mux is
  port(sel    : in  std_logic;
       w0     : in  std_logic_vector(31 downto 0);
       w1     : in  std_logic_vector(31 downto 0);
       output : out std_logic_vector(31 downto 0)
       );
end mux;

architecture behavioral of mux is
begin
  with sel select
    output <= w0                       when '0',
    w1                                 when '1',
    "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" when others;
end behavioral;
