----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:    mem_interface - Behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

-- mem_mode type
use work.util_pkg.all;

entity mem_interface is
  generic(
    M_DATA_WIDTH    : integer := 16;
    M_ADDR_WIDTH    : integer := 4;
    M_MODE          : mem_mode := read
);

  port (
    clk_i   : in std_logic;
    waddr_i  : in std_logic_vector(M_ADDR_WIDTH-1 downto 0);
    raddr_i  : in std_logic_vector(M_ADDR_WIDTH-1 downto 0);
    re_i    : in std_logic;
    we_i    : in std_logic;
    data_i : in std_logic_vector(M_DATA_WIDTH-1 downto 0);
    data_o : out std_logic_vector(M_DATA_WIDTH-1 downto 0)
    );
end mem_interface;
