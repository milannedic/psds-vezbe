----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    13:13:23 06/03/2017
-- Design Name:
-- Module Name:    top_level - Behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity top_level is
  port(
    clk_i   : in std_logic;
    r0_i    : in std_logic;
    r1_i    : in std_logic;
    g0_o    : out std_logic;
    g1_o		: out std_logic
);
end top_level;

architecture Behavioral of top_level is

begin

  arbiter_inst : entity work.arbiter(Behavioral)
		port map(
      clk_i     => clk_i,
      -- --------------------------------
      r0_i      => r0_i,
      r1_i      => r1_i,
      g0_o      => g0_o,
      g1_o      => g1_o
      );

end Behavioral;

