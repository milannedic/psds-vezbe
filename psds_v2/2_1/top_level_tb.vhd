
--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:
-- Project Name:

-- https://stackoverflow.com/questions/34814600/vhdl-test-bench-generics
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;

entity top_level_tb is
end top_level_tb;

architecture behavior of top_level_tb is

  component top_level
    port ( en_i :       in STD_LOGIC;
           reset_i :    in STD_LOGIC;
           clk_i :      in STD_LOGIC;
           pulse_o : out std_logic;
           q_o :          out STD_LOGIC_VECTOR (11 downto 0)
           );

  end component;

  -- constant MAX_VAL : integer := 17;
  --Inputs
  signal clk_i : std_logic := '0';
  signal en_i : std_logic := '1';
  signal reset_i : std_logic := '0';

  --Outputs
  signal q_o : std_logic_vector(11 downto 0);
  signal pulse_o : std_logic;

  -- Clock period definitions
  constant clk_period : time := 10 ns;

  begin

  -- Instantiate the Unit Under Test (UUT)
    uut_top_level:
    entity work.top_level(Structural)
      port map (
        clk_i => clk_i,
        en_i => en_i,
        reset_i => reset_i,
        pulse_o => pulse_o,
        q_o => q_o
        );

  -- Clock process definitions
  clk_process : process
  begin
    clk_i <= '0';
      wait for clk_period/2;
    clk_i <= '1';
      wait for clk_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    reset_i <= '1', '0' after 20 ns;
    en_i <= '0', '1' after 100 ns;
    wait;
  end process;

end;

