----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/23/2018 09:53:10 PM
-- Design Name:
-- Module Name: dec_counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use ieee.numeric_std.all;


entity dec_counter is
    Port ( en_i :       in STD_LOGIC;
           reset_i :    in STD_LOGIC;
           clk_i :      in STD_LOGIC;
           q_o :          out STD_LOGIC_VECTOR (3 downto 0);
           pulse_o :      out STD_LOGIC);
end dec_counter;

architecture Behavioral of dec_counter is
signal r_reg : std_logic_vector(3 downto 0);
signal r_next : std_logic_vector(3 downto 0);
begin

COUNTER: process(clk_i)
begin
    if(reset_i = '1') then
        r_reg <= (others => '0');
    elsif (clk_i'event and clk_i = '1') then
        if (en_i = '1') then
            r_reg <= r_next;
        end if;
    end if;
end process;

    r_next  <= (others => '0') when r_reg = "1001" else r_reg+'1';
    pulse_o <= '1' when r_reg = "1001" else '0';

    q_o <= r_reg;

end Behavioral;
