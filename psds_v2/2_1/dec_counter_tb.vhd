--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:
-- Project Name:

-- https://stackoverflow.com/questions/34814600/vhdl-test-bench-generics
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;

-- clog2b function
use work.util_pkg.all;

entity dec_counter_tb is
end dec_counter_tb;

architecture behavior of dec_counter_tb is

  component dec_counter
    port ( en_i :       in STD_LOGIC;
           reset_i :    in STD_LOGIC;
           clk_i :      in STD_LOGIC;
           q_o :          out STD_LOGIC_VECTOR (3 downto 0);
           pulse_o :      out STD_LOGIC);

  end component;

  -- constant MAX_VAL : integer := 17;
  --Inputs
  signal clk_i : std_logic := '0';
  signal en_i : std_logic := '0';
  signal reset_i : std_logic := '0';

  --Outputs
  signal pulse_o : std_logic;
  signal q_o : std_logic_vector(3 downto 0);

  -- Clock period definitions
  constant clk_period : time := 10 ns;

  begin

  -- Instantiate the Unit Under Test (UUT)
    uut_dec_counter:
    entity work.dec_counter(behavioral)
      port map (
        clk_i => clk_i,
        en_i => en_i,
        reset_i => reset_i,
        pulse_o => pulse_o,
        q_o => q_o
        );

  -- Clock process definitions
  clk_process : process
  begin
    clk_i <= '0';
      wait for clk_period/2;
    clk_i <= '1';
      wait for clk_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    reset_i <= '1', '0' after 20 ns;
    en_i <= '0', '1' after 100 ns;
    wait;
  end process;

end;
