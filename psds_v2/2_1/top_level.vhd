
----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    19:19 25/10/2018
-- Design Name:
-- Module Name:    top_level - Behavioral

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity top_level is
	port(
		clk_i	      : in std_logic;
		en_i		    : in std_logic;
		reset_i		  : in std_logic;
		pulse_o       : out std_logic;
		q_o		    : out std_logic_vector(11 downto 0)
	);
end top_level;
architecture Structural of top_level is
  signal counter_val : std_logic_vector(11 downto 0);

  signal reset_s : std_logic;

  signal lsb_block_en_s, mid_block_en_s, msb_block_en_s : std_logic;
  signal lsb_block_pulse_s, mid_block_pulse_s, msb_block_pulse_s : std_logic;

  -- signal q_lsb, q_mid, q_msb : std_logic_vector(3 downto 0);
  -- signal q_lsb_next, q_mid_next, q_msb_next : std_logic_vector(3 downto 0);

  signal r_reg_top, r_reg_top_next : std_logic_vector(11 downto 0);
begin

  reset_counter: process(clk_i)
  begin
    if(clk_i'event and clk_i = '1') then

      r_reg_top <= r_reg_top_next;

    end if;
  end process;

  mid_block_en_s <= '1' when lsb_block_pulse_s = '1' else '0';
  msb_block_en_s <= '1' when (mid_block_pulse_s = '1') and (lsb_block_pulse_s = '1') else '0';

  lsb_block_en_s <= en_i;

  -- top level structural counter model
	lsb_nibble : entity work.dec_counter(Behavioral)
		port map(
        clk_i     => clk_i,
        -- --------------------------------
        en_i      => lsb_block_en_s,
        reset_i   => reset_s,
        pulse_o   => lsb_block_pulse_s,
        --q_o       => q_lsb
        q_o       => counter_val(3 downto 0)
		);

	mid_nibble : entity work.dec_counter(Behavioral)
		port map(
        clk_i     => clk_i,
        -- --------------------------------
        en_i      => mid_block_en_s,
        reset_i   => reset_s,
        pulse_o   => mid_block_pulse_s,
        --q_o       => q_mid
        q_o       => counter_val(7 downto 4)
      );

	msb_nibble : entity work.dec_counter(Behavioral)
		port map(
        clk_i     => clk_i,
        -- --------------------------------
        en_i      => msb_block_en_s,
        reset_i   => reset_s,
        pulse_o   => msb_block_pulse_s,
        --q_o       => q_msb
        q_o       => counter_val(11 downto 8)
      );

  r_reg_top_next <= (others => '0') when
                    r_reg_top = conv_std_logic_vector(40, 12) else counter_val;
  reset_s <= '1' when r_reg_top = conv_std_logic_vector(40, 12) else reset_i;

  q_o <= r_reg_top;

  -- pulse_o is set when all dec_counter modules have pulse_o set at the same time
  pulse_o <= msb_block_pulse_s and mid_block_pulse_s and lsb_block_pulse_s;

end Structural;
