----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:    arbiter - Behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

  entity arbiter_v3 is
  port (
    clk_i: in std_logic;
    r0_i : in std_logic;
    r1_i : in std_logic;
    g0_o : out std_logic;
    g1_o : out std_logic
    );
  end arbiter_v3;

architecture Behavioral of arbiter_v3 is
  type state_type is (waitr, grant0, grant1);
  signal state_reg, state_next : state_type;
begin
  -- this process implements sequential logic
  SEQ: process(clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      state_reg <= state_next;
    end if;
  end process;

  COMB: process(state_reg, state_next, r0_i, r1_i) is
  begin
    -- initialize grant signals to zero
    g0_o <= '0';
    g1_o <= '0';

    case state_reg is
      when waitr =>
        if r0_i = '0' and r1_i = '0' then
          state_next <= waitr;
        else
          if r1_i = '1' then
            state_next <= grant1;
            -- g1_o <= '1';
          else
            if r1_i = '0' and r0_i = '1' then
              state_next <= grant0;
              -- g0_o <= '1';
            else
              -- keep the waitr state when r0_i = '0'
              state_next <= waitr;
            end if;
          end if;
        end if;

      when grant0 =>
        -- stay in grat0 until r0 is high
        if r0_i = '1' then
          state_next <= grant0;
          g0_o <= '1';
        -- get back to waitr only if there is no other grant request
        elsif r0_i = '0' and r1_i = '0' then
          state_next <= waitr;
        -- jump to grant1 if there is r1 request and at the same time
        -- r0 becomes zero.
        elsif r0_i ='0' and r1_i = '1' then
          state_next <= grant1;
          g1_o <= '1';
        end if;

      when grant1 =>
        -- stay in grat0 until r0 is high
        if r1_i = '1' then
          state_next <= grant1;
          g1_o <= '1';
        -- get back to waitr only if there is no other grant request
        elsif r0_i = '0' and r1_i = '0' then
          state_next <= waitr;
        -- jump to grant1 if there is r1 request and at the same time
        -- r0 becomes zero.
        elsif r0_i ='1' and r1_i = '0' then
          state_next <= grant0;
          g0_o <= '1';
        end if;
    end case;
  end process;
end Behavioral;
