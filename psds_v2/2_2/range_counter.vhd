----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    16:56:03 04/25/2017
-- Design Name:
-- Module Name:    m to n counter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
use ieee.numeric_std.all;

-- clog2b function
use work.util_pkg.all;

entity range_counter is
	generic(
    M : integer := 3;
    N : integer := 22
  );

  port (
          clk             : in std_logic;
          en_i          : in std_logic;
          overflow_o    : out std_logic;
          reset_i       : in std_logic;
          q_o           : out std_logic_vector(clog2b(N+1)-1 downto 0)
        );
end range_counter;

architecture Behavioral of range_counter is
signal count_s : std_logic_vector(clog2b(N+1)-1 downto 0);

-- bit_num gives the minimum number of bits needed to
-- represent upper limit
constant bit_num : integer := clog2b(N+1);

begin
  process(clk)
  begin
    if(clk'event and clk='1') then
      overflow_o <= '0';

      if reset_i='1' then

      -- put smaller value into bit_num of bigger number
      count_s <= conv_std_logic_vector(M, bit_num);
      else
        if en_i ='1' then
          if count_s < conv_std_logic_vector(N, bit_num) then
            --if conv_integer(count_s) < 89 then 		-- identicna provera
            count_s <= count_s + '1';
          else
            overflow_o <= '1';
            count_s <= conv_std_logic_vector(M, bit_num);
          end if;
        end if;
      end if;
    end if;
  end process;
  q_o <= count_s;
end Behavioral;
