

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:   Memory Controller - Behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity sram_mem_controller is
  port (
    clk_i       : in std_logic;
    mem_i       : in std_logic;
    rw_i        : in std_logic;
    ready_o     : out std_logic;
    we_n_o      : out std_logic;
    oe_n_o      : out std_logic;
    tri_n_o     : out std_logic;
    r_addr_en_o : out std_logic;
    r_f2s_en_o  : out std_logic;
    r_s2f_en_o  : out std_logic
    );
  end sram_mem_controller;

  architecture Behavioral of sram_mem_controller is
  type state_type is (idle, r1, r2, r3, w1, w2, w3);
  signal state_reg, state_next : state_type;
begin

  -- this process implements FSM sequential logic
  SEQ: process(clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      state_reg <= state_next;
    end if;
  end process;

  COMB: process(state_reg, state_next, mem_i, rw_i) is
  begin
    -- initialize grant signals to zero
    oe_n_o <= '1';
    we_n_o <= '1';
    ready_o <= '0';
    tri_n_o <= '1';
    r_addr_en_o <= '0';
    r_f2s_en_o <= '0';
    r_s2f_en_o <= '0';

    case state_reg is
      when idle =>
        ready_o <= '1';
        if mem_i = '1' then
          if rw_i = '1' then
            state_next <= r1;
          else
            state_next <= w1;
          end if;
        else
          state_next <= idle;
        end if;
      when w1 =>
        r_addr_en_o <= '1';
        r_f2s_en_o <= '1';
        state_next <= w2;
      when w2 =>
        we_n_o <= '0';
        tri_n_o <= '0';
        state_next <= w3;
      when w3 =>
        tri_n_o <= '0';
        state_next <= idle;
      when r1 =>
        r_addr_en_o <= '1';
        state_next <= r2;
      when r2 =>
        oe_n_o <= '0';
        state_next <= r3;
      when r3 =>
        oe_n_o <= '0';
        r_s2f_en_o <= '1';
        state_next <= idle;
    end case;
  end process;
end Behavioral;
