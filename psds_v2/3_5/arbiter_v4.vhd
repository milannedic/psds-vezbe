

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:    arbiter - Behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

  entity arbiter_v4 is
  port (
    clk_i       : in std_logic;
    cnt_reset_i : in std_logic;
    r0_i        : in std_logic;
    r1_i        : in std_logic;
    g0_o        : out std_logic;
    g1_o        : out std_logic
    );
  end arbiter_v4;

architecture Behavioral of arbiter_v4 is
  type state_type is (waitr0, waitr1, grant0, grant1);
  signal state_reg, state_next : state_type;
  signal r_reg : std_logic_vector(3 downto 0) := (others => '0');
  signal r_next : std_logic_vector(3 downto 0) := (others => '0');
  signal timeout_s : std_logic;
  signal cnt_reset_s : std_logic;
begin

  -- each overflow (timeout_s) pulse causes state switch
  -- between grant0 and grant1, with waitr_ state between them.
  COUNTER: process(clk_i)
  begin
    if (clk_i'event and clk_i = '1') then
        r_reg <= r_next;
    end if;
  end process;

  r_next  <= (others => '0') when
             (r_reg = "1001" or cnt_reset_s = '1') else r_reg+'1';
  timeout_s <= '1' when r_reg = "1001" else '0';

  -- this process implements FSM sequential logic
  SEQ: process(clk_i) is
  begin
    if clk_i'event and clk_i = '1' then
      state_reg <= state_next;
    end if;
  end process;

  COMB: process(state_reg, state_next, r0_i, r1_i, timeout_s) is
  begin
    -- initialize grant signals to zero
    g0_o <= '0';
    g1_o <= '0';

    case state_reg is
      when waitr0 =>
        if (r0_i = '0' and r1_i = '0') then
          state_next <= waitr0;
        else
          if r0_i = '1' then
            state_next <= grant0;
          else
            if (r0_i = '0' and r1_i = '1') then
              state_next <= grant1;
            end if;
          end if;
        end if;

      when waitr1 =>
        if (r0_i = '0' and r1_i = '0') then
          state_next <= waitr1;
        else
          if r1_i = '1' then
            state_next <= grant1;
          else
            if (r0_i = '1' and r1_i = '0') then
              state_next <= grant0;
            end if;
          end if;
        end if;

      when grant0 =>
        -- reset counter on state init
        cnt_reset_s <= '1';
        if r0_i = '0' or timeout_s = '1' then
          state_next <= waitr1;
        else
          state_next <= grant0;
          -- deactivate reset signal after first state reinit
          cnt_reset_s <= '0';
          g0_o <= '1';
        end if;

      when grant1 =>
        cnt_reset_s <= '1';
        if r1_i = '0' or timeout_s = '1' then
          state_next <= waitr0;
        else
          state_next <= grant1;
          cnt_reset_s <= '0';
          g1_o <= '1';
        end if;
    end case;
  end process;
end Behavioral;
