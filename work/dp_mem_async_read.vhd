library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;


entity dp_mem_async_read is
  generic (
    WIDTH      : integer := 8;
    SIZE       : integer := 16;
    ASYNC_READ : boolean := true
    );
  port (
    clk       : in  std_logic;
    reset     : in  std_logic;
    -- Port 1 interface
    p1_addr_i : in  std_logic_vector(log2c(SIZE)-1 downto 0);
    p1_data_i : in  std_logic_vector(WIDTH-1 downto 0);
    p1_data_o : out std_logic_vector(WIDTH-1 downto 0);
    p1_wr_i   : in  std_logic;
    -- Port 2 interface
    p2_addr_i : in  std_logic_vector(log2c(SIZE)-1 downto 0);
    p2_data_i : in  std_logic_vector(WIDTH-1 downto 0);
    p2_data_o : out std_logic_vector(WIDTH-1 downto 0);
    p2_wr_i   : in  std_logic
    );
end entity;
architecture beh of dp_mem_async_read is
  type mem_t is array (0 to SIZE-1) of std_logic_vector(WIDTH-1 downto 0);
  signal mem_s : mem_t;
begin
  process (clk, p1_addr_i, p2_addr_i)
  begin
    if (clk'event and clk = '1') then
      if (reset = '1') then
        mem_s <= (others => (others => '0'));
      else
        if (p1_wr_i = '1') then
          mem_s(to_integer(unsigned(p1_addr_i))) <= p1_data_i;
        end if;

        if (p2_wr_i = '1') then
          mem_s(to_integer(unsigned(p2_addr_i))) <= p2_data_i;
        end if;

        -- if ASYNC_READ = false then
        --   p1_data_o <= mem_s(to_integer(unsigned(p1_addr_i)));
        --   p2_data_o <= mem_s(to_integer(unsigned(p2_addr_i)));
        -- end if;

      end if;
    end if;
  end process;

  -- in case we use async-read memory
  -- process (p1_addr_i, p2_addr_i) is
  -- begin
  --   if ASYNC_READ = true then
      p1_data_o <= mem_s(to_integer(unsigned(p1_addr_i)));
      p2_data_o <= mem_s(to_integer(unsigned(p2_addr_i)));

  --   else
  --     p1_data_o <= (others => '0');
  --     p2_data_o <= (others => '0');

  --   end if;
  -- end process;

end architecture beh;

