library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


entity add_and_shift_mult is
  generic (
    WIDTH: integer := 8
    );
  port (
    -- Clocking and reset interface
    clk: in std_logic;
    reset: in std_logic;
    -- Input data interface
    a_in: in std_logic_vector(WIDTH-1 downto 0);
    b_in: in std_logic_vector(WIDTH-1 downto 0);
    -- Output data interface
    r_out: out std_logic_vector(2*WIDTH-1 downto 0);
    -- Command interface
    start: in std_logic;
    -- Status interface
    ready: out std_logic
    );
end entity;

architecture behavioral of add_and_shift_mult is
  type state_type is (idle, add, shift);
  signal state_reg, state_next: state_type;
  signal a_reg, a_next: std_logic_vector(WIDTH-1 downto 0);
  signal b_reg, b_next: std_logic_vector(WIDTH-1 downto 0);
  signal n_reg, n_next: std_logic_vector(WIDTH-1 downto 0);
  signal p_reg, p_next: std_logic_vector(2*WIDTH-1 downto 0);
begin
-- state and data registers
process (clk, reset)
begin
  if reset = '1' then
    state_reg <= idle;
    a_reg <= (others => '0');
    b_reg <= (others => '0');
    n_reg <= (others => '0');
    p_reg <= (others => '0');
  elsif (clk'event and clk = '1') then
    state_reg <= state_next;
    a_reg <= a_next;
    b_reg <= b_next;
    n_reg <= n_next;
    p_reg <= p_next;
  end if;
end process;

-- combinatorial circuits
process (state_reg, start, a_in, b_in, a_reg, b_reg, p_reg, n_reg, n_next, b_next)
begin
  -- default assignments
  a_next <= a_reg;
  b_next <= b_reg;
  n_next <= n_reg;
  p_next <= p_reg;
  ready <= '0';

  case state_reg is
    when idle =>
      ready <= '1';
      if start = '1' then
        a_next <= a_in;
        b_next <= b_in;
        p_next <= conv_std_logic_vector(0, 2*WIDTH);
        n_next <= conv_std_logic_vector(WIDTH, WIDTH);
        if (b_in(0) = '1') then
          state_next <= add;
        else
          state_next <= shift;
        end if;
      else
        state_next <= idle;
      end if;
    when add =>
      p_next <= p_reg + (conv_std_logic_vector(0, WIDTH)&a_reg);
      state_next <= shift;
    when shift =>
      b_next <= '0'&b_reg(WIDTH-1 downto 1);
      n_next <= n_reg - 1;
      a_next <= a_reg(WIDTH-2 downto 0)&'0';
      if n_next /= conv_std_logic_vector(0, WIDTH) then
        if b_next(0) = '1' then
          state_next <= add;
        else
          state_next <= shift;
        end if;
      else
        state_next <= idle;
      end if;
  end case;
end process;

-- system output
r_out <= std_logic_vector(p_reg);
end behavioral;
