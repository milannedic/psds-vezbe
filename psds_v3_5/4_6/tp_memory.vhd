library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_Std.all;
use work.util_pkg.all;

entity tp_memory is
  generic (
    WIDTH : integer := 8;
    SIZE  : integer := 3
    );
  port (
    clk       : in  std_logic;
    reset     : in  std_logic;
    -- Port 1 interface
    p1_addr_i : in  unsigned(7 downto 0);
    p1_data_i : in  unsigned (WIDTH-1 downto 0);
    p1_data_o : out unsigned(WIDTH-1 downto 0);
    p1_wr_i   : in  std_logic;
    -- Port 2 interface
    p2_addr_i : in  unsigned(7 downto 0);
    p2_data_i : in  unsigned(WIDTH-1 downto 0);
    p2_data_o : out unsigned(WIDTH-1 downto 0);
    p2_wr_i   : in  std_logic;
    -- Port 3 interface
    p3_addr_i : in  unsigned(7 downto 0);
    p3_data_i : in  unsigned(WIDTH-1 downto 0);
    p3_data_o : out unsigned(WIDTH-1 downto 0);
    p3_wr_i   : in  std_logic);
end entity;
architecture beh of tp_memory is
  type mem_t is array (0 to SIZE-1) of
    unsigned(WIDTH-1 downto 0);
  signal mem_s : mem_t;
begin
  process (clk)
  begin
    if (clk'event and clk = '1') then
      if (reset = '1') then
        mem_s <= (others => (others => '0'));
      else
        if (p1_wr_i = '1') then
          mem_s(to_integer(p1_addr_i)) <= p1_data_i;
        end if;
        if (p2_wr_i = '1') then
          mem_s(to_integer(p2_addr_i)) <= p2_data_i;
        end if;
        if (p3_wr_i = '1') then
          mem_s(to_integer(p3_addr_i)) <= p3_data_i;
        end if;
        p1_data_o <= mem_s(to_integer(p1_addr_i));
        p2_data_o <= mem_s(to_integer(p2_addr_i));
        p3_data_o <= mem_s(to_integer(p3_addr_i));
      end if;
    end if;
  end process;
end architecture beh;
