-- original algorithm
-- ------------------------
-- for (i=0; i<n_in-1; i++)
--  for (j=i+1; j<n_in; j++)
--   if (dir_in = 0) then
--     if (x_in1[j] < x_in2[i]) then
--     {
--         x_out1[j] = x_in2[i];
--         x_out2[i] = x_in1[j];
--     }
--   else
--     if(x_in1[j] > x_in2[i]) then
--     {
--         x_out1[j] = x_in2[i];
--         x_out2[i] = x_in1[j];
--     }



library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util_pkg.all;

-- dual port memory
--    each port read/write
--    p1_wr
--    p2_wr
--    write --> syncm, rising
--    read --> async
entity selection_sort is
  generic (
    WIDTH    : integer := 8;
    ARR_SIZE : integer := 30
    );
  port (
    --------------- Clocking and reset interface ---------------
    clk   : in std_logic;
    reset : in std_logic;

    -- Memory interface
    addr1_o : out unsigned(7 downto 0);
    addr2_o : out unsigned(7 downto 0);
    -------------- Data to be written to memory ----------------
    x1_o    : out unsigned(WIDTH-1 downto 0);
    x2_o    : out unsigned(WIDTH-1 downto 0);
    -------------- Data read from memory -----------------------
    x1_i    : in  unsigned(WIDTH-1 downto 0);
    x2_i    : in  unsigned(WIDTH-1 downto 0);

    -- 1 = write
    wr1_o : out std_logic;
    wr2_o : out std_logic;

    -- array size
    n_i   : in unsigned(7 downto 0);
    -- sort direction
    dir_i : in std_logic;

    --------------------- Command interface --------------------
    start : in  std_logic;
    --------------------- Status interface ---------------------
    ready : out std_logic
    );
end entity;

architecture behavioral of selection_sort is
  type state_type is (idle, op, jinit, innercheck, asc, desc);  --add, rdrop, ldrop);
  signal state_reg, state_next : state_type;

  signal i_reg, i_next : unsigned(WIDTH-1 downto 0) := (others => '0');
  signal j_reg, j_next : unsigned(WIDTH-1 downto 0) := (others => '0');

begin

-- State and data registers
  SEQ : process (clk, reset)
  begin
    if reset = '1' then
      state_reg <= idle;
      i_reg     <= (others => '0');
      j_reg     <= (others => '0');
    elsif (clk'event and clk = '1') then
      state_reg <= state_next;
      i_reg     <= i_next;
      j_reg     <= j_next;
    end if;
  end process;

-- Combinatorial circuits
  COMB : process (state_reg, start, n_i, dir_i, i_reg, i_next,
                  j_next, j_next, x1_i, x2_i)
  begin
    -- Default assignments
    i_next <= i_reg;
    j_next <= j_reg;
    -- Default variable values
    -- addr1_o <= (others => '0');
    -- addr2_o <= (others => '0');
    wr1_o <= '0';
    wr2_o <= '0';
    x1_o <= (others => '0');
    x2_o <= (others => '0');
    ready <= '0';


    case state_reg is
      when idle =>
        ready <= '1';
        if start = '1' then
          i_next     <= to_unsigned(0, i_next'length);
          state_next <= jinit;
        else
          state_next <= idle;
        end if;

      when jinit =>
        if i_next < n_i - 1 then
          j_next     <= i_next + 1;  -- i will do +1 in the next state. (state re-use)
          state_next <= innercheck;
        else
          state_next <= idle;
        end if;

      when innercheck =>
        -- j_next <= j_reg + 1;

        if j_next < n_i then
          state_next <= op;
          addr1_o <= i_reg;
          addr2_o <= j_reg;

        else
          i_next      <= i_reg + 1;
          state_next <= jinit;
        end if;

      when op =>
        -- we need to read the memory in order to compare
        wr1_o   <= '0';
        wr2_o   <= '0';
        -- addr1_o <= j_reg;
        -- addr2_o <= i_reg;
        if dir_i = '0' then
          -- now the x1_i and x2_i have values read from addr1 and addr2
          if x1_i < x2_i then
            state_next <= desc;
          else
            -- do nothing, execute new loop iteration
            state_next <= innercheck;
            j_next <= j_reg + 1;
          end if;
        else
          if x1_i > x2_i then
            state_next <= asc;
          else
            -- do nothing, execute new loop iteration
            state_next <= innercheck;
            j_next <= j_reg + 1;

          end if;
        end if;

      when asc =>
        wr1_o <= '1';
        wr2_o <= '1';
        -- addr1_o <= j_next;
        -- addr2_o <= i_next;
        x1_o <= x2_i;
        x2_o <= x1_i;

        state_next <= innercheck;

     when desc =>
        wr1_o <= '1';
        wr2_o <= '1';
        -- addr1_o <= j_next;
        -- addr2_o <= i_next;
        x1_o <= x2_i;
        x2_o <= x1_i;
        state_next <= innercheck;
    end case;
  end process;
end behavioral;
