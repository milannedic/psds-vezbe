--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:
-- Design Name:
-- Module Name:
-- Project Name:

-- https://stackoverflow.com/questions/34814600/vhdl-test-bench-generics
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;

-- clog2b function
use work.util_pkg.all;

entity repetitive_addition_mult_tb is
end repetitive_addition_mult_tb;
-- entity repetitive_addition_mult is
--   generic (WIDTH: positive := 8);
--   port (
--     -- Clocking and reset interface
--     clk: in std_logic;
--     reset: in std_logic;
--     -- Input data interface
--     a_in: in std_logic_vector(WIDTH-1 downto 0);
--     b_in: in std_logic_vector(WIDTH-1 downto 0);
--     -- Output data interface
--     r_out: out std_logic_vector(2*WIDTH-1 downto 0);
--     -- Command interface
--     start: in std_logic;
--     -- Status interface
--     ready: out std_logic);
-- end entity;


architecture behavior of repetitive_addition_mult_tb is

  component repetitive_addition_mult
    -- Component Declaration for the Unit Under Test (UUT)
    generic(
      WIDTH : integer := 8
    );
    port (
        clk : in  std_logic;
        reset : in  std_logic;
        a_in : in  std_logic_vector(WIDTH-1 downto 0);
        b_in : in  std_logic_vector(WIDTH-1 downto 0);

        r_out : out  std_logic_vector(2*WIDTH-1 downto 0);
        start : in std_logic;
        ready           : out std_logic
      );
  end component;


  constant G_WIDTH : integer := 8;
  --Inputs
  signal clk : std_logic := '0';
  signal reset : std_logic := '0';
  signal start : std_logic := '0';
  signal a_in  : std_logic_vector(G_WIDTH-1 downto 0) := x"06";
  signal b_in  : std_logic_vector(G_WIDTH-1 downto 0) := x"05";

  --Outputs
  signal r_out : std_logic_vector(2*G_WIDTH-1 downto 0) := (others => '0');
  signal ready : std_logic := '0';

  -- Clock period definitions
  constant clk_period : time := 10 ns;

  begin

  -- Instantiate the Unit Under Test (UUT)
    uut_repetitive_addition_mult:
    entity work.repetitive_addition_mult(behavioral)
      generic map (
        WIDTH => G_WIDTH)
      port map (
        clk => clk,
        reset => reset,
        start => start,
        ready => ready,
        a_in => a_in,
        b_in => b_in,
        r_out => r_out
        );

  -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
      wait for clk_period/2;
    clk <= '1';
      wait for clk_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    reset <= '1', '0' after 20 ns;
    start <= '0', '1' after 50 ns;
    wait;
  end process;

end;
