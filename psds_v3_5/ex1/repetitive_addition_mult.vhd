library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity repetitive_addition_mult is
  generic (WIDTH: positive := 8);
  port (
    -- Clocking and reset interface
    clk: in std_logic;
    reset: in std_logic;
    -- Input data interface
    a_in: in std_logic_vector(WIDTH-1 downto 0);
    b_in: in std_logic_vector(WIDTH-1 downto 0);
    -- Output data interface
    r_out: out std_logic_vector(2*WIDTH-1 downto 0);
    -- Command interface
    start: in std_logic;
    -- Status interface
    ready: out std_logic);
end entity;

architecture behavioral of repetitive_addition_mult is
type state_type is (idle, ab0, load, op);
signal state_reg, state_next: state_type;
signal a_is_0, b_is_0, count_0: std_logic;
signal a_reg, a_next: std_logic_vector(WIDTH-1 downto 0);
signal n_reg, n_next: std_logic_vector(WIDTH-1 downto 0);
signal r_reg, r_next: std_logic_vector(2*WIDTH-1 downto 0);
signal adder_out: std_logic_vector (2*WIDTH-1 downto 0);
signal sub_out: std_logic_vector(WIDTH-1 downto 0);
begin

-- control path: state register
process (clk, reset)
begin
  if reset = '1' then
    state_reg <= idle;
  elsif (clk'event and clk = '1') then
    state_reg <= state_next;
  end if;
end process;

-- control path: next-state/output logic
process (state_reg, start, a_is_0, b_is_0, count_0)
begin
  case state_reg is
    when idle =>
      if start = '1' then
        if (a_is_0 = '1' or b_is_0 = '1') then
          state_next <= ab0;
        else
          state_next <= load;
        end if;
      else
        state_next <= idle;
      end if;
    when ab0 =>
      state_next <= idle;
    when load =>
      state_next <= op;
    when op =>
      if count_0 = '1' then
        state_next <= idle;
      else
        state_next <= op;
    end if;
  end case;
end process;

-- control path: output logic
ready <= '1' when state_reg = idle else '0';
-- datapath: data register

process (clk, reset)
begin
  if reset = '1' then
    a_reg <= (others => '0');
    n_reg <= (others => '0');
    r_reg <= (others => '0');
  elsif (clk'event and clk='1') then
    a_reg <= a_next;
    n_reg <= n_next;
    r_reg <= r_next;
  end if;
end process;

-- datapath: routing multiplexer
process (state_reg, a_reg, n_reg, r_reg, a_in, b_in, adder_out, sub_out)
begin
  case state_reg is
    when idle =>
      a_next <= a_reg;
      n_next <= n_reg;
      r_next <= r_reg;
    when ab0 =>
      a_next <= std_logic_vector(a_in);
      n_next <= std_logic_vector(b_in);
      r_next <= (others => '0');
    when load =>
      a_next <= std_logic_vector (a_in);
      n_next <= std_logic_vector (b_in);
      r_next <= (others => '0');
    when op =>
      a_next <= a_reg;
      n_next <= sub_out;
      r_next <= adder_out;
  end case;
end process;

  -- datapath: functional units
  adder_out <= (conv_std_logic_vector(0, WIDTH) & a_reg) + r_reg;
  sub_out <= n_reg - 1;

  -- datapath: status
  a_is_0 <= '1' when a_in = conv_std_logic_vector(0, WIDTH) else '0';
  b_is_0 <= '1' when b_in = conv_std_logic_vector(0, WIDTH) else '0';
  count_0 <= '1' when n_next = conv_std_logic_vector(0, WIDTH) else '0';

  -- datapath: output
  r_out <= std_logic_vector(r_reg);
end behavioral;
