----------------------------------------------------------------------------------
-- company:
-- engineer:
--
-- create date:   16.11.2018 01:53
-- design name:
-- module name:    comparator - behavioral

----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity comparator is

	generic(
		WIDTH : integer := 8
    );
	port (
		op1_i			: in std_logic_vector(WIDTH - 1 downto 0);
		op2_i			: in std_logic_vector(WIDTH - 1 downto 0);
		eq_o			: out std_logic;
		lt_o			: out std_logic;
		gt_o			: out std_logic
    );
end comparator;

architecture behavioral of comparator is

begin
	comparator : process(op1_i, op2_i) is
	begin
    lt_o <= '0';
    gt_o <= '0';
    eq_o <= '0';

		if (op1_i < op2_i) then
			lt_o <= '1';
    elsif (op1_i > op2_i) then
      gt_o <= '1';
    else
			eq_o <= '1';
		end if;
	end process;

end behavioral;
