# This script takes arguments and passes them to tbgen.py
# python script which will generate dut_tb.vhd file

# run it like:
# ./tbgen.sh psds_v2 3_2 arbiter

# parameters:
# 1 - class
# 2 - problem
# 3 - design unit, entity to test

class=$1
problem=$2
dut=$3

ROOT=$(pwd)

python ./tbgen/tbgen.py $ROOT $class $problem $dut

#echo $ROOT
