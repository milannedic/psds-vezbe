library ieee;
use ieee.std_logic_1164.all;

package util_pkg is
	function clog2b(n:integer) return integer;	
	function log2c(n:integer) return integer;

  -- memory mode, first time used in 3.2
  type mem_mode is (read, write, rburst, wburst);

end util_pkg;

package body util_pkg is
	function clog2b(n:integer) return integer is
		variable m,p:integer;
	begin
	m:=0;
	p:=1;
	while p<n loop
		m := m + 1;
		p := p * 2;
	end loop;
	return m;
	end clog2b;

-- the exact same function as above, some .vhd design files
-- use log2c and some the other one.
function log2c(n:integer) return integer is
		variable m,p:integer;
	begin
	m:=0;
	p:=1;
	while p<n loop
		m := m + 1;
		p := p * 2;
	end loop;
	return m;
	end log2c;
end util_pkg;

